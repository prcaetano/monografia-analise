# Monografia-analise

Este repositório contêm o código e os dados referentes à análise e geração de figuras da dissertação de mestrado "Descrevendo a relação entre galáxias e matéria escura em 'pequenas' escalas: um estudo de modelos de ocupação de halos usando processos gaussianos e otimização bayesiana".

Os notebooks utilizados para geração das figuras estão no diretório "notebooks". Para executá-los, é necessário descomprimir os dados e importar alguns repositórios, o que é feito pelo script `setup.sh`. O ambiente conda utilizado pode ser reproduzido a partir do arquivo `environment.yml`.

Os dados usados na análise foram gerados utilizando múltiplos códigos, que podem ser encontrados nos repositórios:

* https://gitlab.com/prcaetano/cosmosis - implementa emulação por processos gaussianos ao [CosmoSIS](https://bitbucket.org/joezuntz/cosmosis)
* https://gitlab.com/prcaetano/hod-mock - módulo CosmoSIS que implementa geração de mocks populando catálogos de halos e cálculo da função de correlação (usando [halotools](https://halotools.readthedocs.io/en/latest/) e [treecorr](https://rmjarvis.github.io/TreeCorr/_build/html/overview.html))
* https://gitlab.com/prcaetano/hod - módulo CosmoSIS que calcula analiticamente a função de correlação angular a partir de modelo de HOD, baseado na implementação de João Paulo Nogueira Cavalcante e Paulo Pellegrini.

Os arquivos de configuração e job scripts usados para a geração das cadeias e diagnósticos dos emuladores podem ser encontrados no diretório "jobs", embora não haja documentação.

