#!/bin/bash

source config_1000S.sh

export MODULE_LIST=${MODULE_LIST_TRAINED}
export IS_TRAINED="T"

for ((i=0; i<4; i++)); do
    export EMULATED_COV_NAME="./data/input/sim_cov_zbin_045_060_full_redmagic_y1_cov_1perc_cosmic_variance_sigmam_0p38_massmin_12p67_alpha_1p00_mass0_12p04_mass1_13p91_fcentral_0p76_v2_added_emulation_${TEST_CODE}.dat"
    compute_corrected_covariance.py ${PIPELINE_FNAME} ${EMULATED_COV_NAME}

    cat ${SAMPLE_FILES} sample_files.ini | grep -v "cov =" > sample_files_corrected_covariance.ini
    echo "cov=${EMULATED_COV_NAME}" >> sample_files_corrected_covariance.ini

    export SAMPLER="minuit"
    export MINUIT_BESTFIT_FNAME=${MINUIT_BESTFIT_FNAME_WITH_EMU_COV}
    export MINUIT_COV_FNAME=${MINUIT_COV_FNAME_WITH_EMU_COV}
    export SAMPLE_FILES="sample_files_corrected_covariance.ini"
    nice -n 10 cosmosis ${PIPELINE_FNAME}
done

export SAMPLER="emcee"
export PARAMS_FNAME=${MINUIT_BESTFIT_FNAME}
export OUTPUT_FNAME=${OUTPUT_FNAME_WITH_EMU_COV}
export SAMPLE_COV=${SAMPLE_COV_WITH_EMU_COV}
export START_POINTS=${START_POINTS_WITH_EMU_COV}

nice -n 10 cosmosis ${PIPELINE_FNAME}

