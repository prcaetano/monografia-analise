#!/bin/bash

source config_1000S.sh

export SAMPLER="emulatortrainer"
export MODULE_LIST=${MODULE_LIST_TRAINING}
export IS_TRAINED="F"
nice -n 10 cosmosis ${PIPELINE_FNAME}

rm ./code_version &> /dev/null
for dir in $(find ./code/ -mindepth 1 -maxdepth 1 -type d -printf "%P\n") ;
do
    pushd ./code/$dir > /dev/null
    echo "$dir: $(git rev-parse HEAD)" >> ../../code_version
    popd > /dev/null
done

