#!/bin/bash

source config_1000S.sh

export MODULE_LIST=${MODULE_LIST_TRAINED}
export IS_TRAINED="T"

export SAMPLER="minuit"
export MINUIT_BESTFIT_FNAME=${MINUIT_BESTFIT_FNAME_NO_EMU_COV}
export MINUIT_COV_FNAME=${MINUIT_COV_FNAME_NO_EMU_COV}
nice -n 10 cosmosis ${PIPELINE_FNAME}

export SAMPLER="emcee"
export PARAMS_FNAME=${MINUIT_BESTFIT_FNAME}
export OUTPUT_FNAME=${OUTPUT_FNAME_NO_EMU_COV}
export SAMPLE_COV=${SAMPLE_COV_NO_EMU_COV}
export START_POINTS=${START_POINTS_NO_EMU_COV}

nice -n 10 cosmosis ${PIPELINE_FNAME}

rm ./code_version &> /dev/null
for dir in $(find ./code/ -mindepth 1 -maxdepth 1 -type d -printf "%P\n") ;
do
    pushd ./code/$dir > /dev/null
    echo "$dir: $(git rev-parse HEAD)" >> ../../code_version
    popd > /dev/null
done

