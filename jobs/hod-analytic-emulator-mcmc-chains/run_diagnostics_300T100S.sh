#!/bin/bash

source config_300T100S.sh

export SAMPLER="emulatortrainer"
export MODULE_LIST=${MODULE_LIST_TRAINING}
export IS_TRAINED="F"

#run_trained_pipeline.py ${PIPELINE_FNAME} ${VALIDATION_DESIGN} ${VALIDATION_DATA_EMULATED} --trained
#run_trained_pipeline.py ${PIPELINE_FNAME} ${VALIDATION_DESIGN} ${VALIDATION_DATA_SIMULATED} --not_trained
#run_trained_pipeline.py ${PIPELINE_FNAME} ${TRAINING_DESIGN} ${TRAINING_DATA_EMULATED} --trained

#diagnostics.py ${TRAINING_DATA_SIMULATED} ${TRAINING_DATA_EMULATED} "${TEST_CODE}_testing" ${EMULATOR_FILE}
diagnostics.py ${VALIDATION_DATA_SIMULATED} ${VALIDATION_DATA_EMULATED} "${TEST_CODE}_validation"

