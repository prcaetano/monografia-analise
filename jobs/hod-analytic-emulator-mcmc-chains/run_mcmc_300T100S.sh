#!/bin/bash
export PATH=${JOB_ROOT_DIR}/code/:$PATH

export PIPELINE_FNAME=pipeline_with_density_bo_template.ini

export CHAIN_NUMBER="1"
export RUN="R2"


export NSAMPLES="100"
export NSTEP="100"
export TEST_CODE="FID_noPCA_${NSAMPLES}S_${RUN}"
export KERNEL="exp_squared"
export WEIGHT_PCA="T"
export NPCA=30
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"

export DESIGN="${HOD_DATA_PATH}/input/design_${NSAMPLES}_analytic_emulator_2.npy"
export ACQUISITION_COVARIANCE="./data/input/FID_noPCA_100S_R2_with_emulation_covariance_1sigma.txt"
export PARAMETER_COVARIANCE="./data/input/FID_noPCA_300T100S_R1_with_emulation_covariance_1sigma.txt"
export SAMPLE_COV="./data/input/FID_noPCA_100S_R1_with_emulation_covariance_2sigma.txt"

export TRAINING_FILE="${HOD_OUT_DATA_PATH}/HODPCAanACF${NSAMPLES}S10R6P_${RUN}.fits"
export EMULATOR_FILE="${HOD_OUT_DATA_PATH}/HODPCAanACF${NSAMPLES}S10R6P_${RUN}_${TEST_CODE}.pkl"


export MODULE_LIST_TRAINING="consistency camb sigma8_rescale halofit hmc_density hmc limber noise 2pt"
export MODULE_LIST_TRAINED="noise"
export SHORTCUT="hmc_density"
export IS_TRAINED="F"
export SAMPLER="emulatortrainer"
export PARAMS_FNAME="params.ini"
export SAMPLE_FILES="sample_files.ini"

export MODULE_LIST=$MODULE_LIST_TRAINING
nice -n 10 cosmosis ${PIPELINE_FNAME}
export MODULE_LIST=$MODULE_LIST_TRAINED


#export MODULE_LIST="consistency camb sigma8_rescale halofit hmc_density noise 2pt"
#export SHORTCUT="hmc_density"
#export IS_TRAINED="T"
#
#export SAMPLER="minuit"
#export MINUIT_BESTFIT_FNAME="./bestfits/${TEST_CODE}_minuit_bestfit_no_theoretical_covariance.ini"
#export MINUIT_COV_FNAME="./covariances/${TEST_CODE}_minuit_covariance_no_theoretical_covariance.txt"
#export PARAMS_FNAME="params.ini"
#export SAMPLE_FILES="sample_files.ini"
#nice -n 10 cosmosis ${PIPELINE_FNAME}
#
#
#export SAMPLER="emcee"
#export EMCEE_WALKERS=108
#export EMCEE_SAMPLES=4000
#export EMCEE_NSTEPS=10
#export PARAMS_FNAME=${MINUIT_BESTFIT_FNAME}
#export OUTPUT_FNAME="./data/out/chains/${TEST_CODE}_emcee_no_theoretical_covariance_${CHAIN_NUMBER}.txt"
##export SAMPLE_COV="./data/input/FID_noPCA_300T100S_R1_no_emulation_covariance_2sigma.txt"
#export SAMPLE_COV="./data/input/FID_noPCA_100S_R1_no_emulation_covariance_2sigma.txt"
##export START_POINTS="./data/out/FID_noPCA_300T100S_emcee_no_theoretical_covariance_1.txt"
##export START_POINTS="./data/out/chains/${TEST_CODE}_emcee_no_theoretical_covariance_${CHAIN_NUMBER}_start.txt"
#nice -n 10 cosmosis ${PIPELINE_FNAME}
#
#
#
#for ((i=0; i<4; i++)); do
#    export PARAMS_FNAME=${MINUIT_BESTFIT_FNAME}
#    export EMULATED_COV_NAME="./data/input/sim_cov_zbin_045_060_full_redmagic_y1_cov_1perc_cosmic_variance_sigmam_0p38_massmin_12p67_alpha_1p00_mass0_12p04_mass1_13p91_fcentral_0p76_v2_added_emulation_${TEST_CODE}.dat"
#    compute_corrected_covariance.py ${PIPELINE_FNAME} ${EMULATED_COV_NAME}
#
#    cat ${SAMPLE_FILES} sample_files.ini | grep -v "cov =" > sample_files_corrected_covariance.ini
#    echo "cov=${EMULATED_COV_NAME}" >> sample_files_corrected_covariance.ini
#
#    export SAMPLER="minuit"
#    export MINUIT_BESTFIT_FNAME="./bestfits/${TEST_CODE}_minuit_bestfit_with_theoretical_covariance.ini"
#    export MINUIT_COV_FNAME="./covariances/${TEST_CODE}_minuit_covariance_with_theoretical_covariance.txt"
#    export SAMPLE_FILES="sample_files_corrected_covariance.ini"
#    nice -n 10 cosmosis ${PIPELINE_FNAME}
#done
#
#export SAMPLER="emcee"
#export EMCEE_WALKERS=108
#export EMCEE_SAMPLES=4000
#export EMCEE_NSTEPS=10
#export PARAMS_FNAME=${MINUIT_BESTFIT_FNAME}
#export OUTPUT_FNAME="./data/out/chains/${TEST_CODE}_emcee_with_theoretical_covariance_${CHAIN_NUMBER}.txt"
##export SAMPLE_COV="./data/input/FID_noPCA_300T100S_R1_with_emulation_covariance_2sigma.txt"
#export SAMPLE_COV="./data/input/FID_noPCA_100S_R1_no_emulation_covariance_2sigma.txt"
##export START_POINTS="./data/out/chains/${TEST_CODE}_emcee_with_theoretical_covariance_1.txt"
#
#nice -n 10 cosmosis ${PIPELINE_FNAME}
#
#
#rm ./code_version &> /dev/null
#for dir in $(find ./code/ -mindepth 1 -maxdepth 1 -type d -printf "%P\n") ;
#do
#    pushd ./code/$dir > /dev/null
#    echo "$dir: $(git rev-parse HEAD)" >> ../../code_version
#    popd > /dev/null
#done
#
