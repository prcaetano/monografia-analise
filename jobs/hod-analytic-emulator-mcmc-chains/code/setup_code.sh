#/usr/bin/env bash
git clone --depth 1 --branch e20cdb5263b20725904c87925c0a185d4e54f896 https://bitbucket.org/prcaetano/cosmosis.git
cd ./cosmosis
git clone --depth 1 --branch bbafc5af7de75f152526ef1e8213449a08956884 https://bitbucket.org/joezuntz/cosmosis-standard-library
cd ../
git clone --depth 1 --branch fb14c1254fd74a6ad139e5fbe9cb4049c3913759 https://bitbucket.org/prcaetano/hod.git
