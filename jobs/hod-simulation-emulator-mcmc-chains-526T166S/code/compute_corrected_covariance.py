#!/usr/bin/env python
import argparse
from cosmosis.runtime.config import Inifile
from cosmosis.runtime.pipeline import LikelihoodPipeline, Pipeline
import numpy as np


def get_corrected_covariance(pipeline_fname):
    """
    Runs pipeline and returns the data covariance matrix added to the
    emulated one.

    Parameters:
        pipeline_fname: str
            path to ini file describing the pipeline

    Returns:
        cov

        cov: np.ndarray
            corrected data covariance matrix
    """
    override_dict = {
                     ('hod_acf', 'is_trained'): 'T',
                     ('runtime', 'sampler'): 'test',
                    }

    ini = Inifile(pipeline_fname, override=override_dict)
    pipeline = LikelihoodPipeline(ini, override=override_dict)

    pars = []
    for par in pipeline.parameters:
        val = par.start
        pars.append(val)

    data = pipeline.run_parameters(pars, all_params=True)

    theo_cov = data["data_vector", "hod_cov"]
    data_cov = data["data_vector", "hod_covariance"]

    return data_cov[:-1,:-1] + theo_cov


if __name__ == "__main__":
    ## Parsing args
    parser = argparse.ArgumentParser(description="Gets corrected covariance,"
                                    "including emulation errors")
    parser.add_argument("pipeline", help="path to cosmosis pipeline")
    parser.add_argument("output", help="path to file that will hold the computed covariance")

    args = parser.parse_args()

    pipeline_fname = args.pipeline
    output_fname = args.output

    ## Running pipeline
    cov = get_corrected_covariance(pipeline_fname)

    ## Saving
    np.savetxt(output_fname, cov.reshape(-1, 1))

