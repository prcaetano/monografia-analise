#/usr/bin/env bash
git clone --depth 1 --branch 0c25a1f3eab90d04d3734a2ea5a9358e105dcad5 https://bitbucket.org/prcaetano/cosmosis.git
cd ./cosmosis
git clone --depth 1 --branch bd73c4981b19a30239436d54a33f89d53b70f7f2 https://bitbucket.org/joezuntz/cosmosis-standard-library
cd ../
git clone --depth 1 --branch 11f7d2021fb4a394047a6948632334e3d1c63b6d https://prcaetano@bitbucket.org/prcaetano/hod-mock.git
