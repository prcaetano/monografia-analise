#!/bin/bash
export PATH=/home/drc01/sobreira/pcaetano/hod-runs/hod-simulation-emulator-mcmc-chains-1/:$PATH

export PIPELINE_FNAME=pipeline_bo.ini
export CHAIN_NUMBER="1"
export RUN="R1"
export NTOTAL="526"
export NSAMPLES="166"
export NREPEATS="1"
export NSTEP="36"
export TEST_CODE="FID_SIM_${NTOTAL}T${NSAMPLES}S_${RUN}"

export KERNEL="exp_squared"
export WEIGHT_PCA="T"
export NPCA=30
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"

export DESIGN="${HOD_DATA_PATH}/input/design_${NSAMPLES}S.npy"
export ACQUISITION_COVARIANCE="./data/input/FID_noPCA_166S_R4_with_emulation_covariance_1sigma.txt"
export PARAMETER_COVARIANCE="./data/input/FID_noPCA_300T100S_R1_with_emulation_covariance_1sigma.txt"

export TRAINING_FILE="${HOD_OUT_DATA_PATH}/HODsim${NTOTAL}T${NSAMPLES}S10R6P_${RUN}.fits"
export EMULATOR_FILE="${HOD_OUT_DATA_PATH}/HODsim${NTOTAL}T${NSAMPLES}S10R6P_${RUN}_${TEST_CODE}.pkl"

export MODULE_LIST_TRAINING="hod_mock hod_acf hod_ngal 2pt"
export MODULE_LIST_TRAINED="hod_acf hod_ngal 2pt"
export SHORTCUT=""
export PARAMS_FNAME="params_166.ini"
export SAMPLE_FILES="sample_files.ini"

#EMCEE CONFIGS
export EMCEE_WALKERS=108
export EMCEE_SAMPLES=10000
export EMCEE_NSTEPS=10

#export SAMPLE_COV_NO_EMU_COV="./data/input/FID_noPCA_166S_R4_with_emulation_covariance_2sigma.txt"
export SAMPLE_COV_NO_EMU_COV=""
export START_POINTS_NO_EMU_COV=""
#export SAMPLE_COV_NO_EMU_COV=""
#export START_POINTS_NO_EMU_COV="./data/out/chains/${TEST_CODE}_emcee_no_theoretical_covariance_1.txt"

#export SAMPLE_COV_WITH_EMU_COV="./data/input/FID_noPCA_166S_R4_with_emulation_covariance_2sigma.txt"
export SAMPLE_COV_WITH_EMU_COV=""
export START_POINTS_WITH_EMU_COV=""
#export SAMPLE_COV_WITH_EMU_COV=""
#export START_POINTS_WITH_EMU_COV="./data/out/chains/${TEST_CODE}_emcee_with_theoretical_covariance_1.txt"

export OUTPUT_FNAME_NO_EMU_COV="./data/out/chains/${TEST_CODE}_emcee_no_theoretical_covariance_${CHAIN_NUMBER}.txt"
export OUTPUT_FNAME_WITH_EMU_COV="./data/out/chains/${TEST_CODE}_emcee_with_theoretical_covariance_${CHAIN_NUMBER}.txt"


#MINUIT CONFIGS
export MINUIT_BESTFIT_FNAME_NO_EMU_COV="./bestfits/${TEST_CODE}_minuit_bestfit_no_theoretical_covariance.ini"
export MINUIT_COV_FNAME_NO_EMU_COV="./covariances/${TEST_CODE}_minuit_covariance_no_theoretical_covariance.txt"
export MINUIT_BESTFIT_FNAME_WITH_EMU_COV="./bestfits/${TEST_CODE}_minuit_bestfit_with_theoretical_covariance.ini"
export MINUIT_COV_FNAME_WITH_EMU_COV="./covariances/${TEST_CODE}_minuit_covariance_with_theoretical_covariance.txt"


#DIAGNOSTICS CONFIGS
export VALIDATION_DESIGN="./data/input/design_126S.npy"
export TRAINING_DESIGN="./data/input/design_526T166S.npy"
export VALIDATION_DATA_EMULATED="./data/out/HODsim${NTOTAL}T${NSAMPLES}S10R6P_${RUN}_trained_emulated_${TEST_CODE}_validation.npy"
export VALIDATION_DATA_SIMULATED="./data/out/HODsim${NTOTAL}T${NSAMPLES}S10R6P_${RUN}_trained_simulated_${TEST_CODE}_validation.npy"
export TRAINING_DATA_EMULATED="./data/out/HODsim${NTOTAL}T${NSAMPLES}S10R6P_${RUN}_trained_emulated_${TEST_CODE}_training.npy"
export TRAINING_DATA_SIMULATED=${TRAINING_FILE}

