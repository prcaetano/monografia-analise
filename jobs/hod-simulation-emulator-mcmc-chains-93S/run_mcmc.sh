#!/bin/bash
export PATH=${JOB_ROOT_DIR}/code/:$PATH

export PIPELINE_FNAME="pipeline.ini"
export NSAMPLES="93"
export CHAIN_NUMBER="2"

export WEIGHT_PCA="T"
export NPCA=30

#Test b3p1_sim93
export TEST_CODE="FID_noPCA_sim_${NSAMPLES}"
export KERNEL="exp_squared"
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
export ACQUISITION_COVARIANCE="./data/out/b3p1_100_with_emulation_cov_3sigma.txt"
export PARAMETER_COVARIANCE="./data/out/b3p1_100_no_emulation_cov_5sigma.txt"
export SAMPLE_COV="./data/out/b3p1_100_with_emulation_cov_2sigma.txt"

#export MODULE_LIST_TRAINING="hod_mock hod_acf 2pt"
export MODULE_LIST_TRAINING="hod_acf 2pt"
export MODULE_LIST_TRAINED="hod_acf hod_ngal 2pt"
export SHORTCUT=""
export IS_TRAINED="F"
export SAMPLER="emulatortrainer"
export PARAMS_FNAME="params_93.ini"
export SAMPLE_FILES="sample_files.ini"


export MODULE_LIST=$MODULE_LIST_TRAINING
cosmosis ${PIPELINE_FNAME}
export MODULE_LIST=$MODULE_LIST_TRAINED

export MODULE_LIST=${MODULE_LIST_TRAINED}
export SHORTCUT=""
export IS_TRAINED="T"

export SAMPLER="minuit"
export MINUIT_BESTFIT_FNAME="${TEST_CODE}_minuit_bestfit_no_theoretical_covariance.ini"
export MINUIT_COV_FNAME="${TEST_CODE}_minuit_covariance_no_theoretical_covariance.txt"
export PARAMS_FNAME="params_93.ini"
export SAMPLE_FILES="sample_files.ini"
cosmosis ${PIPELINE_FNAME}


export SAMPLER="emcee"
export EMCEE_WALKERS=108
export EMCEE_SAMPLES=10000
export EMCEE_NSTEPS=10
export PARAMS_FNAME=${MINUIT_BESTFIT_FNAME}
export OUTPUT_FNAME="./data/out/chains/${TEST_CODE}_emcee_no_theoretical_covariance_${CHAIN_NUMBER}.txt"
export START_COV="./data/out/b3p1_100_no_emulation_cov_2sigma.txt"
nice -n 10 cosmosis ${PIPELINE_FNAME}



for ((i=0; i<4; i++)); do
    export PARAMS_FNAME=${MINUIT_BESTFIT_FNAME}
    export EMULATED_COV_NAME="./data/input/ACFHODsimulatedMice100realizationsWithEmulationCovSigmam_0p6Mmin_12p5Alpha_1p0M0_12p5M1_13p9Fcen0p55_${TEST_CODE}.dat"
    compute_corrected_covariance.py ${PIPELINE_FNAME} ${EMULATED_COV_NAME}

    cat ${SAMPLE_FILES} sample_files.ini | grep -v "cov =" > sample_files_corrected_covariance.ini
    echo "cov=${EMULATED_COV_NAME}" >> sample_files_corrected_covariance.ini

    export SAMPLER="minuit"
    export MINUIT_BESTFIT_FNAME="${TEST_CODE}_minuit_bestfit_with_theoretical_covariance.ini"
    export MINUIT_COV_FNAME="${TEST_CODE}_minuit_covariance_with_theoretical_covariance.txt"
    export SAMPLE_FILES="sample_files_corrected_covariance.ini"
    cosmosis ${PIPELINE_FNAME}
done

export SAMPLER="emcee"
export EMCEE_WALKERS=108
export EMCEE_SAMPLES=10000
export EMCEE_NSTEPS=10
export PARAMS_FNAME=${MINUIT_BESTFIT_FNAME}
export ADD_THEORETICAL_COVARIANCE="F"
export OUTPUT_FNAME="./data/out/chains/${TEST_CODE}_emcee_with_theoretical_covariance_${CHAIN_NUMBER}.txt"
export START_COV="./data/out/b3p1_100_with_emulation_cov_2sigma.txt"
nice -n 10 cosmosis ${PIPELINE_FNAME}


rm ./code_version &> /dev/null
for dir in $(find ./code/ -mindepth 1 -maxdepth 1 -type d -printf "%P\n") ;
do
    pushd ./code/$dir > /dev/null
    echo "$dir: $(git rev-parse HEAD)" >> ../../code_version
    popd > /dev/null
done

