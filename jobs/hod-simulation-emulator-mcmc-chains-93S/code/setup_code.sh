#/usr/bin/env bash
git clone --depth 1 --branch 8de842f55e25bd6d91f62eb9b2c18d60f3d2a799 https://bitbucket.org/prcaetano/cosmosis.git
cd ./cosmosis
git clone --depth 1 --branch bd73c4981b19a30239436d54a33f89d53b70f7f2 https://bitbucket.org/joezuntz/cosmosis-standard-library
cd ../
git clone --depth 1 --branch 52604c3365433bb8b2f6c37f3890c728024350e6 https://bitbucket.org/prcaetano/hod-mock.git

