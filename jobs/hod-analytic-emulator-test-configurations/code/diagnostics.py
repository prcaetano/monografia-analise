#!/usr/bin/env python
import argparse
import numpy as np
import pickle
import pymultinest
import os
from astropy.table import Table
from astropy.io import fits
from sklearn.metrics import explained_variance_score, mean_squared_error, r2_score


def load_training_data(fname):
    hdul = fits.open(fname)
    nrepeats = hdul[0].header["NREPEATS"]

    tab_y = Table.read(fname, hdu=2)
    data_y = np.lib.recfunctions.structured_to_unstructured(tab_y.as_array())

    tab_x = Table.read(fname, hdu=1)
    data_x = np.lib.recfunctions.structured_to_unstructured(tab_x.as_array()).flatten()

    data_y_mean = np.r_[[data_y[k*nrepeats:(k+1)*nrepeats].mean(axis=0) for k in range(int(len(data_y)/nrepeats))]]
    data_y_var = np.r_[[data_y[k*nrepeats:(k+1)*nrepeats].var(axis=0) for k in range(int(len(data_y)/nrepeats))]]

    return np.repeat(np.atleast_2d(data_x), 10, axis=0), data_y_mean, np.sqrt(data_y_var)


def bic(pickle_fname):
    with open(pickle_fname, "rb") as f:
        emulated_module = pickle.load(f)

    bic_value = 0
    for gp, y in zip(emulated_module.gps, np.atleast_2d(emulated_module._output)):
        npars = len(gp.kernel.get_parameter_vector())
        bic_value += npars * np.log(len(y))- 2*gp.log_likelihood(y)
    return bic_value


def evidence(pickle_fname):
    lnZ_all = 0
    lnZ_all_err = 0

    with open(pickle_fname, "rb") as f:
        emulated_module = pickle.load(f)

    for gp, y in zip(emulated_module.gps, emulated_module._output):
        n_params = len(gp.get_parameter_vector())
        def prior(cube, ndim, nparams):
            return

        def loglike(cube, ndim, nparams):
            p = np.zeros(n_params)
            for i in range(n_params):
                p[i] = cube[i]
            gp.set_parameter_vector(p)
            return gp.log_likelihood(y)

        pymultinest.run(loglike, prior, n_params, outputfiles_basename='multinest_',
                        resume=False, verbose=True)
        a = pymultinest.Analyzer(outputfiles_basename='multinest_', n_params=n_params)
        lnZ = a.get_stats()['global evidence']
        lnZ_err = a.get_stats()['global evidence error']
        lnZ_all += lnZ
        lnZ_all_err += lnZ_err
    os.system('rm -f multinest_*')
    return lnZ_all, lnZ_all_err


def diagnostics(ys_simulated, ys_emulated, pickle_fname, test_name):
    with open("r2_scores.txt", "a") as f:
        print("{}, {:.3f}".format(test_name, r2_score(ys_simulated, ys_emulated)), file=f)
    with open("msd_scores.txt", "a") as f:
        print("{}, {:.5e}".format(test_name, mean_squared_error(ys_simulated, ys_emulated)),
              file=f)
    with open("explained_variance_scores.txt", "a") as f:
        print("{}, {:.5f}".format(test_name, explained_variance_score(ys_simulated, ys_emulated)),
              file=f)
    if pickle_fname is not None:
        #with open("evidences.txt", "a") as f:
        #    lnZ, lnZ_err = evidence(pickle_fname)
        #    print("{}, {:.3f}, {:.3f}".format(test_name, lnZ, lnZ_err), file=f)

        with open("bic_scores.txt", "a") as f:
            print("{}, {:.4f}".format(test_name, bic(pickle_fname)), file=f)


if __name__ == "__main__":
    ## Parsing args
    parser = argparse.ArgumentParser(description="Calculate diagnostics for training  "
                                                 "or validation samples.")
    parser.add_argument("model_data_fname", help="Training/validation data for model")
    parser.add_argument("emulated_model_data_fname", help="Training/validation data for surrogate model")
    parser.add_argument("test_name", help="Name of test")
    parser.add_argument("emulated_module_pickle_fname", help="Pickle file holding the emulated model", nargs="?", const=None)

    args = parser.parse_args()

    training_data_fname = args.model_data_fname
    emulated_training_data_fname = args.emulated_model_data_fname
    pickle_fname = args.emulated_module_pickle_fname
    test_name = args.test_name

    xs_training_emulated, ys_training_emulated, yerrs_training_emulated = \
            np.load(emulated_training_data_fname)
    xs_training, ys_training, yerrs_training = load_training_data(training_data_fname)

    diagnostics(ys_training, ys_training_emulated, pickle_fname, test_name)


