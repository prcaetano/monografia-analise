#!/usr/bin/env python
import pymultinest
import os


def evidence(pickle_fname):
    lnZ_all = 0
    lnZ_all_err = 0

    with open(pickle_fname, "rb") as f:
        emulated_module = pickle.load(f)

    for gp, y in zip(emulated_module.gps, emulated_module._output):
        n_params = len(gp.get_parameter_vector())
        def prior(cube, ndim, nparams):
            return

        def loglike(cube, ndim, nparams):
            p = np.zeros(n_params)
            for i in range(n_params):
                p[i] = cube[i]
            gp.set_parameter_vector(p)
            return gp.log_likelihood(y)

        pymultinest.run(loglike, prior, n_params, outputfiles_basename='multinest_',
                        resume=False, verbose=True)
        a = pymultinest.Analyzer(outputfiles_basename='multinest_', n_params=n_params)
        lnZ = a.get_stats()['global evidence']
        lnZ_err = a.get_stats()['global evidence error']
        lnZ_all += lnZ
        lnZ_all_err += lnZ_err
    os.system('rm -f multinest_*')
    return lnZ_all, lnZ_all_err


