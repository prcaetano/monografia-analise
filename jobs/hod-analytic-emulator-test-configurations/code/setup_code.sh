#/usr/bin/env bash
git clone --depth 1 --branch 2a9d3197852f900555ee9c72784604f4a1773ee1 https://bitbucket.org/prcaetano/cosmosis.git
cd ./cosmosis
git clone --depth 1 --branch bbafc5af7de75f152526ef1e8213449a08956884 https://bitbucket.org/joezuntz/cosmosis-standard-library
cd ../
git clone --depth 1 --branch 5aae026793e6346e22dfde8a4c9889f805c07130 https://bitbucket.org/prcaetano/hod.git
