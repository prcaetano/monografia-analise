#!/usr/bin/env python
import argparse
import numpy as np
from astropy.table import Table


def get_design(fname):
    tab = Table.read(fname, hdu=3)
    arr = np.array(tab.to_pandas())
    return arr


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Extract design from training file"
                                                 "as numpy array")
    parser.add_argument("training_file", help="Training file (fits)")
    parser.add_argument("design_file", help="Design file to be written (npy)")

    args = parser.parse_args()

    arr = get_design(args.training_file)
    np.save(args.design_file, arr)
 
