#!/bin/bash

. env_vars

export RUN_TRAINED_PIPELINE=${JOB_ROOT_DIR}/code/run_trained_pipeline.py
export DIAGNOSTICS=${JOB_ROOT_DIR}/code/diagnostics.py
export PATH=${JOB_ROOT_DIR}/code/:$PATH

export MODULE_LIST_TRAINING="consistency camb sigma8_rescale halofit hmc_density hmc limber noise 2pt"
export MODULE_LIST_TRAINED="noise"
export SHORTCUT=""
export IS_TRAINED="F"
export SAMPLER="emulatortrainer"
export PARAMS_FNAME="params.ini"
export SAMPLE_FILES="sample_files.ini"
export DIAGONALIZE_PARAMETER_COVARIANCE="F"


train_and_test() {
    export MODULE_LIST=$MODULE_LIST_TRAINING
    cosmosis pipeline_with_density_template.ini
    export MODULE_LIST=$MODULE_LIST_TRAINED

    run_trained_pipeline.py pipeline_with_density_template.ini ./data/out/HODPCAanACF2000S10R6P_R1_design.npy ./data/out/HODPCAanACF2000S10R6P_R1_trained_${TEST_CODE}.npy
    run_trained_pipeline.py pipeline_with_density_template.ini ./data/out/HODPCAanACF100S10R6P_R1_design.npy ./data/out/HODPCAanACF100S10R6P_R1_trained_${TEST_CODE}.npy

    diagnostics.py ./data/out/HODPCAanACF100S10R6P_R1.fits ./data/out/HODPCAanACF100S10R6P_R1_trained_${TEST_CODE}.npy "${TEST_CODE}_testing" ./data/out/HODPCAanACF100S10R6P_R1_${TEST_CODE}.pkl
    diagnostics.py ./data/out/HODPCAanACF2000S10R6P_R1.fits ./data/out/HODPCAanACF2000S10R6P_R1_trained_${TEST_CODE}.npy "${TEST_CODE}_validation"
}

rm -f *txt


#Tests organization:

#fiducial: a9p1, b3p1
# FID1, FID2
# variações: kernel (K1, ... K7)
# exp_squared, matern32, linear, exp_squared + matern32, exp_squared + linear, matern32 + linear, exp_squared + matern32 + linear
# log/nolog
# weight/noweight
# fitwhitenoise
# fitmean
# npca 5, 30



export TEST_CODE="FID_PCA"
export KERNEL="exp_squared"
export DO_LOG="T"
export DO_PCA="T"
export WEIGHT_PCA="F"
export NPCA=5
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_PCA_noLog"
export KERNEL="exp_squared"
export DO_LOG="F"
export DO_PCA="T"
export WEIGHT_PCA="F"
export NPCA=5
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_PCA_weight"
export KERNEL="exp_squared"
export DO_LOG="T"
export DO_PCA="T"
export WEIGHT_PCA="T"
export NPCA=5
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_PCA_nowhitenoise"
export KERNEL="exp_squared"
export DO_LOG="T"
export DO_PCA="T"
export WEIGHT_PCA="F"
export NPCA=5
export FIT_WHITE_NOISE="F"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_PCA_nomean"
export KERNEL="exp_squared"
export DO_LOG="T"
export DO_PCA="T"
export WEIGHT_PCA="F"
export NPCA=5
export FIT_WHITE_NOISE="T"
export FIT_MEAN="F"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_PCA_nodiag"
export KERNEL="exp_squared"
export DO_LOG="T"
export DO_PCA="T"
export WEIGHT_PCA="F"
export NPCA=5
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="F"
train_and_test


##KERNELS
export TEST_CODE="FID_PCA_K2"
export KERNEL="matern32"
export DO_LOG="T"
export DO_PCA="T"
export WEIGHT_PCA="F"
export NPCA=5
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_PCA_K3"
export KERNEL="linear"
export DO_LOG="T"
export DO_PCA="T"
export WEIGHT_PCA="F"
export NPCA=5
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_PCA_K4"
export KERNEL="exp_squared + matern32"
export DO_LOG="T"
export DO_PCA="T"
export WEIGHT_PCA="F"
export NPCA=5
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_PCA_K5"
export KERNEL="exp_squared + linear"
export DO_LOG="T"
export DO_PCA="T"
export WEIGHT_PCA="F"
export NPCA=5
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_PCA_K6"
export KERNEL="matern32 + linear"
export DO_LOG="T"
export DO_PCA="T"
export WEIGHT_PCA="F"
export NPCA=5
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_PCA_K7"
export KERNEL="exp_squared + matern32 + linear"
export DO_LOG="T"
export DO_PCA="T"
export WEIGHT_PCA="F"
export NPCA=5
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test



#FID_noPCA
export TEST_CODE="FID_noPCA"
export KERNEL="exp_squared"
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test


##CONFIG
export TEST_CODE="FID_noPCA_noLog"
export KERNEL="exp_squared"
export DO_LOG="F"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_noPCA_nowhitenoise"
export KERNEL="exp_squared"
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="F"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_noPCA_nomean"
export KERNEL="exp_squared"
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="F"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_noPCA_nodiag"
export KERNEL="exp_squared"
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="F"
train_and_test

##KERNELS
export TEST_CODE="FID_noPCA_K2"
export KERNEL="matern32"
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_noPCA_K3"
export KERNEL="linear"
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_noPCA_K4"
export KERNEL="exp_squared + matern32"
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_noPCA_K5"
export KERNEL="exp_squared + linear"
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_noPCA_K6"
export KERNEL="matern32 + linear"
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test

export TEST_CODE="FID_noPCA_K7"
export KERNEL="exp_squared + matern32 + linear"
export DO_LOG="T"
export DO_PCA="F"
export FIT_WHITE_NOISE="T"
export FIT_MEAN="T"
export DIAGONALIZE_PARAMETER_COVARIANCE="T"
train_and_test



rm ./code_version &> /dev/null
for dir in $(find ./code/ -mindepth 1 -maxdepth 1 -type d -printf "%P\n") ;
do
    pushd ./code/$dir > /dev/null
    echo "$dir: $(git rev-parse HEAD)" >> ../../code_version
    popd > /dev/null
done

