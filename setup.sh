#/usr/bin/env bash
echo "Extraindo dados..."
cd ./data
cat *part* > data.tar.bz2
tar -xvf data.tar.bz2
rm *part*
rm *tar*

echo "Obtendo repositórios necessários..."
cd ../code
git clone https://gitlab.com/prcaetano/chain-diagnostics.git
git clone https://gitlab.com/prcaetano/cosmosis.git
cd ./cosmosis
git clone --depth 1 --branch v1.6.2 https://bitbucket.org/joezuntz/cosmosis-standard-library
cd ../
git clone https://gitlab.com/prcaetano/halotools-utils.git
cd ./halotools-utils
git clone https://gitlab.com/prcaetano/jackknife-tools.git
cd ../
git clone --depth 1 --branch plots-mono https://gitlab.com/prcaetano/hod.git
git clone https://gitlab.com/prcaetano/hod-scripting-utils.git
git clone https://gitlab.com/prcaetano/jackknife-tools.git

echo "Feito"
