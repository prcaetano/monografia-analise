import argparse
from cosmosis.runtime.config import Inifile
from cosmosis.runtime.pipeline import LikelihoodPipeline, Pipeline
import numpy as np


def run_pipeline(pipeline_fname, hodpars, use_trained=True):
    """
    Runs pipeline for hod parameters in list hodpars and returns the computed correlation funtions.

    Parameters:
        pipeline_fname: str
            path to ini file describing the pipeline
        hodpars: list of dict
            list of hod parameters to run the pipeline over. Each should be a dict with the
            keys sigmam, massmin, alpha, mass0, mass1 and fcentral
        use_trained: bool
             whether to use the emulated acf or the original simulated acf

    Returns:
        thetas, wthetas, wthetaerrs

        thetas: np.ndarray
            array of values of theta for each hod parameters used
        wthetas: np.ndarray
            array of values of the correlation function for each hod parameter used
        wthetaerrs: np.ndarray
            array with the corresponding estimated uncertainties for wtheta
    """
    if use_trained:
        is_trained = "T"
    else:
        is_trained = "F"
    override_dict = {
                     ('noise', 'is_trained'): is_trained,
                     ('noise', 'skip_setup'): "F",
                     ('runtime', 'sampler'): 'test',
                    }
    thetas = []
    wthetas = []
    wthetaerrs = []

    for key in hodpars[0]:
        override_dict[("hod_parameters", key)] = str(hodpars[0][key])

    ini = Inifile(pipeline_fname, override=override_dict)
    pipeline = LikelihoodPipeline(ini, override=override_dict)

    for hodpar in hodpars:
        pars = []
        for par in pipeline.parameters:
            val = par.limits[0]
            for name in hodpar.keys():
                if "hod_parameters--" + name.lower() == str(par):
                    val = hodpar[name]
            pars.append(val)

        data = pipeline.run_parameters(pars, all_params=True)

        wtheta = data["data_vector", "hod_acf"]
        wthetaerr = data["data_vector", "hod_acf_err"]
        theta = data["data_vector", "hod_theta"]

        thetas.append(theta)
        wthetas.append(wtheta)
        wthetaerrs.append(wthetaerr)

    return np.array(thetas), np.array(wthetas), np.array(wthetaerrs)


if __name__ == "__main__":
    ## Parsing args
    parser = argparse.ArgumentParser(description="Runs a trained pipeline on a set of points "
                                    "to enable comparison with the training data.")
    parser.add_argument("pipeline", help="path to cosmosis pipeline")
    parser.add_argument("design", help="path to file holding the design (points to sample)")
    parser.add_argument("output", help="path to file that will hold the computed values")

    args = parser.parse_args()

    pipeline_fname = args.pipeline
    design_fname = args.design
    output_fname = args.output

    ## Loading hod parameters to use
    design = np.load(design_fname)
    hodpars = [{"sigmam": par[0],
                "massmin": par[1],
                "alpha": par[2],
                "mass0": par[3],
                "mass1": par[4],
                "fcentral": par[5],
               } for par in design]

    ## Running pipeline
    thetas, wthetas, wthetaerrs = run_pipeline(pipeline_fname, hodpars)

    ## Saving
    np.save(output_fname, np.array([thetas, wthetas, wthetaerrs]))

